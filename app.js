const express               = require('express'),
      methodOverride        = require('method-override'),
      bodyParser            = require('body-parser'),
      routes                = require('./routes'),
      app                   = express(),
      Util                  = require('./utils/util')
      PORT                  = 3000,
      HOST                  = '0.0.0.0'


// server config
app.use(methodOverride('X­HTTP­Method'))
app.use(methodOverride('X­HTTP­Method­Override'))
app.use(methodOverride('X­Method­Override'))
app.use(methodOverride('_method'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use((request, response, next) => {
  if (request.url === '/favicon.ico') {
    response.writeHead(200, {'Content-Type': 'image/x-icon'})
    response.end('')
  } else {
    next()
  }
})

// router
app.use('/', routes)

// error handling
app.use((request, response, next) => {
  const err = new Error('Não encontrado')
  err.status = 404
  next(err)
})

app.use((err, request, response, next) => {
  const answer = Util.buildErrorMessage(err.message)
  response.status(err.status || 500).json(answer)
})

const server = app.listen(PORT, () => {
  const host = server.address().address,
        port = server.address().port
  console.log('Desafio backend server listening at http://%s:%s', host, port)
})

module.exports = server