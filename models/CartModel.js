'use strict'

const mongoose = require('mongoose');

function CartDao(model) {
  this.model = model
}

CartDao.prototype.create = function (data, callback) {
  const model = new this.model(data)
  model.save(function (err, result) {
    callback(err, result)
  })
}

CartDao.prototype.find = function (query, callback) {
  this.model
    .find(query)
    .populate({
      path: 'itens.produto',
      model: 'Produto' ,
      select: '-valor -__v -fator'
    })
    .exec(callback)
}

CartDao.prototype.findOne = function (query, callback) {
  this.model
    .findOne(query)
    .populate({
      path: 'itens.produto',
      model: 'Produto',
      select: '-valor -__v -fator'
    })
    .exec(callback)
}

CartDao.prototype.countOne = function (_id, callback) {
  this.model    
    .aggregate([
      { $match: {_id: new mongoose.Types.ObjectId(_id)}},
      { $unwind: "$itens" },
      { $project: {_id: '$_id', fator: '$itens.fator', valor: '$itens.valor'} },
      { $group: { _id: '$fator', count: { $sum: 1 }, total: {$sum: '$valor'} } }
    ], function (err, result) {      
      callback(err, result)
    })
}

CartDao.prototype.update = function (_id, data, callback) {
  const query = { _id: _id }
  this.model.update(query, data).exec(function (err, result) {
    callback(err, result)
  })
}

CartDao.prototype.remove = function (_id, callback) {
  const query = { _id: _id }
  this.model.remove(query).exec(function (err, result) {
    callback(err, result)
  })
}

module.exports = function (mongoose) {
  const itemSchema = new mongoose.Schema({    
    fator:    { type: String, enum: ['A', 'B', 'C'], required: [true, 'O fator de desconto do produto é obrigatório (A, B ou C)'] },
    valor:    { type: Number, required: [true, 'O valor do produto é obrigatório'] },
    produto:  {  type: mongoose.Schema.Types.ObjectId, ref:  'Produtos'}
  })

  const Cart = mongoose.model('Carrinho', {
    itens: [{type: itemSchema}]
  })
  return new CartDao(Cart)
}