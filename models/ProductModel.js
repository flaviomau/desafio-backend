'use strict'

function ProductDao(model) {
  this.model = model
}

ProductDao.prototype.create = function (data, callback) {
  const model = new this.model(data)
  model.save(function (err, result) {
    callback(err, result)
  })
}

ProductDao.prototype.find = function (query, callback) {
  this.model.find(query).exec(callback)
}

ProductDao.prototype.findOne = function (query, callback) {
  this.model.findOne(query).exec(callback)
}

ProductDao.prototype.update = function (_id, data, callback) {
  const query = { _id: _id },
        opts = { runValidators: true }
  this.model.update(query, data, opts).exec(function (err, result) {
    callback(err, result)
  })
}

ProductDao.prototype.remove = function (_id, callback) {
  const query = { _id: _id }
  this.model.remove(query).exec(function (err, result) {
    callback(err, result)
  })
}

module.exports = function (mongoose) {
  const Product = mongoose.model('Produto', {
    nome:       { type: String, required: [true, 'O nome do produto é obrigatório'] },
    descricao:  { type: String, required: [true, 'A descrição do produto é obrigatória'] },
    imagem:     { type: String, required: [true, 'A url da imagem do produto é obrigatória'] },
    valor:      { type: Number, required: [true, 'O valor do produto é obrigatório'] },
    fator:      { type: String, enum: ['A', 'B', 'C'], required: [true, 'O fator de desconto do produto é obrigatório (A, B ou C)']}
  })
  return new ProductDao(Product)
}