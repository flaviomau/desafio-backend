exports.handleNotFound = (data) => {
    if (!data) {
        const err = new Error('Registro não localizado no banco de dados')
        err.status = 404
        throw err
    }
    return data
}

exports.buildSuccessMessage = (message, data) => {
    return {
        success: true,
        message: message || "",
        data: data || ""
    }
}

exports.buildErrorMessage = (message) => {
    return {
        success: false,
        message: message
    }
}

exports.buildError = (message) => {
    const err = new Error(message)
    err.status = 401
    return err
}