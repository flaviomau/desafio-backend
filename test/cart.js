'use stricts'

const app = require('../app'),
      chai = require('chai'),
      request = require('supertest')
      expect = chai.expect

let produtoA = { nome: 'Produto A', descricao: 'Descricao A', imagem: 'imagemA.jpg', valor: 100, fator: 'A' },
  produtoB = { nome: 'Produto B', descricao: 'Descricao B', imagem: 'imagemB.jpg', valor: 200, fator: 'B' },
  produtoC = { nome: 'Produto C', descricao: 'Descricao C', imagem: 'imagemC.jpg', valor: 300, fator: 'C' },
  cart = {}

describe('Desafio backend tests', function () {
  describe('#Carrinho tests', function () {

    describe('#POST / produtos - ProdutoA', function () {
      it('should return new produto data', function (done) {
        request(app)
          .post('/produtos')
          .send(produtoA)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto inserido com sucesso')
            produtoA = res.body.data
            done()
          })
      })
    })

    describe('#POST / produtos - ProdutoB', function () {
      it('should return new produto data', function (done) {
        request(app)
          .post('/produtos')
          .send(produtoB)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto inserido com sucesso')
            produtoB = res.body.data
            done()
          })
      })
    })

    describe('#POST / produtos - ProdutoC', function () {
      it('should return new produto data', function (done) {
        request(app)
          .post('/produtos')
          .send(produtoC)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto inserido com sucesso')
            produtoC = res.body.data
            done()
          })
      })
    })

    describe('#POST / carrinhos', function () {
      it('should return new carrinho data', function (done) {
        request(app)
          .post('/carrinhos')
          .send({produto: produtoA._id})
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de carrinho de compras criado com sucesso')
            cart = res.body.data
            done()
          })
      })
    })

    describe('#GET / carrinhos / id', function () {
      it('should return the carrinho created', function (done) {
        request(app)
          .get('/carrinhos/' + cart._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Dados do carrinho de compras recuperados com sucesso')
            expect(res.body.data._id).to.equal(cart._id)
            expect(res.body.data.quantidadeItens).to.equal(1)
            expect(res.body.data.percentualDesconto).to.equal(1)
            done()
          })
      })
    })

    describe('#POST / carrinhos - second item', function () {
      it('should return success', function (done) {
        request(app)
          .post('/carrinhos/' + cart._id)
          .send({produto: produtoB._id})
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Novo item inserido no carrinho de compras com sucesso')
            done()
          })
      })
    })

    describe('#GET / carrinhos / id', function () {
      it('should return the carrinho created', function (done) {
        request(app)
          .get('/carrinhos/' + cart._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Dados do carrinho de compras recuperados com sucesso')
            expect(res.body.data._id).to.equal(cart._id)
            expect(res.body.data.quantidadeItens).to.equal(2)
            expect(res.body.data.percentualDesconto).to.equal(6)
            done()
          })
      })
    })

    describe('#POST / carrinhos - third item', function () {
      it('should return success', function (done) {
        request(app)
          .post('/carrinhos/' + cart._id)
          .send({produto: produtoC._id})
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Novo item inserido no carrinho de compras com sucesso')
            done()
          })
      })
    })

    describe('#GET / carrinhos / id', function () {
      it('should return the carrinho created', function (done) {
        request(app)
          .get('/carrinhos/' + cart._id)
          .end(function (err, res) {
            itemToDelete = res.body.data.itens[0]._id
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Dados do carrinho de compras recuperados com sucesso')
            expect(res.body.data._id).to.equal(cart._id)
            expect(res.body.data.quantidadeItens).to.equal(3)
            expect(res.body.data.percentualDesconto).to.equal(16)
            done()
          })
      })
    })

    describe('#DELETE / carrinhos - first item', function () {
      it('should return success', function (done) {
        request(app)
          .delete('/carrinhos/' + cart._id + '/' + itemToDelete)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Item removido do carrinho de compras com sucesso')
            done()
          })
      })
    })

    describe('#GET / carrinhos / id', function () {
      it('should return the carrinho created', function (done) {
        request(app)
          .get('/carrinhos/' + cart._id)
          .end(function (err, res) {
            itemToDelete = res.body.data.itens[0]._id
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Dados do carrinho de compras recuperados com sucesso')
            expect(res.body.data._id).to.equal(cart._id)
            expect(res.body.data.quantidadeItens).to.equal(2)
            expect(res.body.data.percentualDesconto).to.equal(15)
            done()
          })
      })
    })

    describe('#POST / carrinhos / checkout', function () {
      it('should return success', function (done) {
        request(app)
          .post('/carrinhos/checkout/123456789012')
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.equal('Registro não localizado no banco de dados')
            done()
          })
      })
    })

    describe('#POST / carrinhos / checkout', function () {
      it('should return success', function (done) {
        request(app)
          .post('/carrinhos/checkout/' + cart._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Checkout efetuado com sucesso')
            done()
          })
      })
    })

    describe('#DELETE / produtos', function () {
      it('should return success', function (done) {
        request(app)
          .delete('/produtos/' + produtoA._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto excluído com sucesso')
            done()
          })
      })
    })

    describe('#DELETE / produtos', function () {
      it('should return success', function (done) {
        request(app)
          .delete('/produtos/' + produtoB._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto excluído com sucesso')
            done()
          })
      })
    })

    describe('#DELETE / produtos', function () {
      it('should return success', function (done) {
        request(app)
          .delete('/produtos/' + produtoC._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto excluído com sucesso')
            done()
          })
      })
    })

  })
})