'use stricts'

const app = require('../app'),
      chai = require('chai'),
      request = require('supertest')
      expect = chai.expect

let produtoteste = { nome: 'Produto 01', descricao: 'Descricao 01', imagem: 'imagem1.jpg', valor: 100, fator: 'A' }

describe('Desafio backend tests', function () {
  describe('#Produto tests', function () {
    
    describe('#POST / produtos / null', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({})
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('O nome do produto é obrigatório,A descrição do produto é obrigatória,A url da imagem do produto é obrigatória,O valor do produto é obrigatório,O fator de desconto do produto é obrigatório (A, B ou C)')
            done()
          })
      })
    })

    describe('#POST / produtos / empty', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({ nome: '', descricao: '', imagem: '', valor: '', fator: '' })
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('O nome do produto é obrigatório,A descrição do produto é obrigatória,A url da imagem do produto é obrigatória,O valor do produto é obrigatório,O fator de desconto do produto é obrigatório (A, B ou C)')
            done()
          })
      })
    })

    describe('#POST / produtos / name empty', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({ nome: '', descricao: 'Descricao 01', imagem: 'imagem1.jpg', valor: 100, fator: 'A' })
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('O nome do produto é obrigatório')
            done()
          })
      })
    })

    describe('#POST / produtos / description empty', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({ nome: 'Produto 01', descricao: '', imagem: 'imagem1.jpg', valor: 100, fator: 'A' })
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('A descrição do produto é obrigatória')
            done()
          })
      })
    })

    describe('#POST / produtos / image empty', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({ nome: 'Produto 01', descricao: 'Descricao 01', imagem: '', valor: 100, fator: 'A' })
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('A url da imagem do produto é obrigatória')
            done()
          })
      })
    })

    describe('#POST / produtos / value empty', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({ nome: 'Produto 01', descricao: 'Descricao 01', imagem: 'imagem1.jpg', fator: 'A' })
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('O valor do produto é obrigatório')
            done()
          })
      })
    })

    describe('#POST / produtos / factor empty', function () {
      it('should return error validation messages', function (done) {
        request(app)
          .post('/produtos')
          .send({ nome: 'Produto 01', descricao: 'Descricao 01', imagem: 'imagem1.jpg', valor: 100, fator: '' })
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.false
            expect(res.body.message).to.be.an('string')
            expect(res.body.message).to.be.equal('O fator de desconto do produto é obrigatório (A, B ou C)')
            done()
          })
      })
    })

    describe('#POST / produtos', function () {
      it('should return new produto data', function (done) {
        request(app)
          .post('/produtos')
          .send(produtoteste)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto inserido com sucesso')
            produtoteste = res.body.data
            done()
          })
      })
    })

    produtoteste.nome = 'produto 01 plus'
    describe('#PUT / produtos - update name', function () {
      it('should return success', function (done) {
        request(app)
          .put('/produtos/' + produtoteste._id)
          .send(produtoteste)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto atualizado com sucesso')
            done()
          })
      })
    })

    describe('#PUT / produtos - null object', function () {
      it('should return error', function (done) {
        request(app)
          .put('/produtos/' + produtoteste._id)
          .send({})
          .end(function (err, res) {
            expect(res.statusCode).to.equal(401)
            expect(res.body.success).to.be.false
            expect(res.body.message).to.equal('O fator de desconto do produto é obrigatório (A, B ou C),O valor do produto é obrigatório,A url da imagem do produto é obrigatória,A descrição do produto é obrigatória,O nome do produto é obrigatório')
            done()
          })
      })
    })

    describe('#GET / produtos / id', function () {
      it('should return the produto created and edited', function (done) {
        request(app)
          .get('/produtos/' + produtoteste._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Busca de registro de produto realizada com sucesso')
            expect(res.body.data._id).to.equal(produtoteste._id)
            done()
          })
      })
    })

    describe('#GET / produtos', function () {
      it('should return the produtos list', function (done) {
        request(app)
          .get('/produtos')
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Listagem de produtos realizada com sucesso')
            expect(res.body.data).to.be.an('array')
            done()
          })
      })
    })

    describe('#DELETE / produtos', function () {
      it('should return success', function (done) {
        request(app)
          .delete('/produtos/' + produtoteste._id)
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body.success).to.be.true
            expect(res.body.message).to.equal('Registro de produto excluído com sucesso')
            done()
          })
      })
    })

  })
})