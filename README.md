# Desafio Backend Developer
A Totvs está criando um aplicativo de e-commerce, foi feita uma planning com o Time o qual você é integrante e a Sprint começou. Suas tarefas são as seguintes: 

##### Criação de Produtos na loja ###
Criar os endpoints para adicionar e listar os produtos da loja. Esse produto deve ter os seguintes atributos: Nome, Descrição, Imagem, Valor e Fator. Existem três fatores A, B e C.  

##### Criação do Carrinho de Compras ###
Criar endpoints para adicionar, consultar e remover os produtos do carrinho de compras. A consulta dos produtos deve conter além da lista, a quantidade e valor total dos produtos adicionados seguindo a seguinte regra de fator

 - Os produtos de fator A tem um desconto progressivo de 1% a cada item adicionado no carrinho limitado a 5% de desconto. 
 - Os produtos de fator B tem um desconto progressivo de 5% a cada item adicionado no carrinho limitado a 15% de desconto.
 - Os produtos de fator C tem um desconto progressivo de 10% a cada item adicionado no carrinho limitado a 30% de desconto.
 - Um detalhe importante, o total de desconto do carrinho de compras não pode superar 30%.

##### Checkout pagar.me ###
Para finalizar, crie um endpoint para fazer o checkout do carrinho de compras através do pagar.me

# Passos para execução do projeto:
### Ferramentas de ambiente
Para a execução no modo de desenvolvimento e dos testes automatizados, é necessário que estejam instalados no ambiente as seguintes ferramentas:
 - Nodejs: 9.11.1 ou superior
 - Mongodb: 3.6.4 ou superior
 - npm: 5.6.0 ou yarn: 1.9.2

Para efetuar o deploy, é necessário que estejam instalados no ambiente as seguintes ferramentas:
- Docker: 18.06.0-ce
- Docker-compose: 1.22.0

Para acessar os endpoints recomenda-se a utilização do aplicativo Postman: 6.2.2 ou superior.

A porta padrão utilizada pelo servidor será a 3000.

#### Desenvolvimento
Para executar o projeto em modo de desenvolvimento, execute o comando ```npm dev``` or ```Yarn dev```

#### Testes automatizados
Para executar os scripts de testes automatizados, execute o comando ```npm test``` or ```Yarn test```

#### Testes automatizados + relatório de cobertura
Para executar os scripts de testes automatizados e gerar um relatório de cobertura dos testes, execute o comando ```npm test-coverage``` or ```Yarn test-coverage```

#### Deploy
Para efetuar o deploy da aplicação em um container Docker, execute o comando ```sudo docker-compose up --build```

# EndPoints:
### Gerenciamento de produtos (/produtos)
#### Listar todos os produtos
 - **URL**: /produtos
 - **Verbo HTTP**: GET
 - **Parâmetro URL**: nenhum
 - **Body request**: nenhum
 - **Body response**: 
```js
  {
    "success":  "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message":  "Mensagem retornada em caso de sucesso/erro",
    "data":[
      {
        "_id":       "SKU do item",
        "nome":       "Nome do produto",
        "descricao":  "Descrição do produto",
        "imagem":     "url do arquivo de imagem do produto",
        "valor":      "Valor do produto",
        "fator":      "Fator de desconto do produto"
      }
   ]
  }
```
#### Pesquisar um produto pelo seu SKU
 - **URL**: /produtos/sku
 - **Verbo HTTP**: GET
 - **Parâmetro URL**: *sku* - número do sku gerado no momento do cadastro do item
 - **Body request**: nenhum
 - **Body response**: 
```js
  {
    "success":  "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message":  "Mensagem retornada em caso de sucesso/erro",
    "data":{
        "_id":        "SKU do item",
        "nome":       "Nome do produto",
        "descricao":  "Descrição do produto",
        "imagem":     "url do arquivo de imagem do produto",
        "valor":      "Valor do produto",
        "fator":      "Fator de desconto do produto"
    }
  }
```
#### Inserir um novo produto
 - **URL**: /produtos
 - **Verbo HTTP**: POST
 - **Parâmetro URL**: nenhum
 - **Body request**:
```js
   {
      "nome":       "Nome do produto",
      "descricao":  "Descrição do produto",
      "imagem":     "url do arquivo de imagem do produto",
      "valor":      "Valor do produto",
      "fator":      "Fator de desconto do produto"
  }
```
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
        "_id":        "SKU do item",
        "nome":       "Nome do produto",
        "descricao":  "Descrição do produto",
        "imagem":     "url do arquivo de imagem do produto",
        "valor":      "Valor do produto",
        "fator":      "Fator de desconto do produto"
    }
  }
```
#### Atualizar um produto
 - **URL**: /produtos/sku
 - **Verbo HTTP**: PUT
 - **Parâmetro URL**: *sku* - número do sku gerado no momento do cadastro do item
 - **Body request**:
```js
   {
      "nome":       "Nome do produto",
      "descricao":  "Descrição do produto",
      "imagem":     "url do arquivo de imagem do produto",
      "valor":      "Valor do produto",
      "fator":      "Fator de desconto do produto"
  }
```
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
        "_id": "número do sku gerado no momento do cadastro do item"
    }
  }
```
#### Apagar um produto
 - **URL**: /produtos/sku
 - **Verbo HTTP**: DELETE
 - **Parâmetro URL**: *sku* - número do sku gerado no momento do cadastro do item
 - **Body request**: Nenhum
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
        "_id": "número do sku gerado no momento do cadastro do item"
    }
  }
```
### Carrinho de compra (/carrinhos)
#### Ler os dados de um carrinho
 - **URL**: /carrinhos/numero
 - **Verbo HTTP**: GET
 - **Parâmetro URL**: *numero* - número do carrinho gerado no momento de sua criação
 - **Body request**: nenhum
 - **Body response**: 
```js
  {
    "success":  "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message":  "Mensagem retornada em caso de sucesso/erro",
    "data": {
      "valorBruto": "Valor bruto do carrinho",
      "percentualDesconto": "Percentual de desconto efetuado no carrinho",
      "valorLiquido": "Valor liquido do carrinho",
      "quantidadeItens": "Quantidade total de itens do carrinho",
      "_id": "Número do carrinho",
      "itens": [
        {
          "_id": "identificador do item dentro do carrinho",
          "produto": {
            "_id": "sku do produto",
            "nome": "nome do produto",
            "descricao": "descrição do item",
            "imagem": "url da imagem do item"
          },
          "fator": "fator de desconto do item",
          "valor": "valor do item"
        }
      ]
    }
  }
```
#### Criar um novo carrinho adicionando um item
 - **URL**: /carrinhos
 - **Verbo HTTP**: POST
 - **Parâmetro URL**: nenhum
 - **Body request**:
```js
  {
      "produto": "sku do produto que será inserido no carrinho"
  }
```
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
      "_id": "numero do carrinho (deve ser utilizado para referenciar este carrinho)",
      "itens": [
        {
          "_id": "Identificador do item dentro do carrinho de compras (para excluir o item, este número deverá ser utilizado)",
          "produto": "sku do produto",
          "fator": "fator de desconto do produto",
          "valor": "valor do produto"
        }
      ]
    }
  }
```
#### Adicionar um novo item em um carrinho já existente
 - **URL**: /carrinhos/numero
 - **Verbo HTTP**: POST
 - **Parâmetro URL**: *numero* - número do carrinho gerado no momento de sua criação
 - **Body request**:
```js
  {
      "produto": "sku do produto que será inserido no carrinho"
  }
```
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
      "_id": "número do carrinho de compras"
    }
  }
```
#### Remover um item de um carrinho
 - **URL**: /carrinhos/numero/identificador
 - **Verbo HTTP**: POST
 - **Parâmetro URL**: *numero* - número do carrinho gerado no momento de sua criação / *identificador* - Identificador do item dentro do carrinho
 - **Body request**:
```js
  {
      "produto": "sku do produto que será inserido no carrinho"
  }
```
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
      "_id": "número do carrinho de compras"
    }
  }
```
#### Efetuar o checkout de um carrinho
- **URL**: /carrinhos/checkout/numero
 - **Verbo HTTP**: POST
 - **Parâmetro URL**: *numero* - número do carrinho gerado no momento de sua criação
 - **Body request**: Nenhum
 - **Body response**: 
```js
  {
    "success": "true - operação efetuada com sucesso / false - erro ao executar a operação",
    "message": "Mensagem retornada em caso de sucesso/erro",
    "data": {
      "_id": "número do carrinho de compras"
    }
  }
```