const createRouter = (ProductModel) => {
  const express = require('express'),
        router = express.Router(),
        ProductController = require('../controllers/ProductController')(ProductModel)

  router.get('/', ProductController.readAll.bind(ProductController))
  router.get('/:_id', ProductController.readById.bind(ProductController))
  router.post('/', ProductController.create.bind(ProductController))
  router.put('/:_id', ProductController.update.bind(ProductController))
  router.delete('/:_id', ProductController.remove.bind(ProductController))
  return router
}

module.exports = createRouter