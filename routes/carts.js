const createRouter = (CartModel, ProductModel) => {
    const express = require('express'),
          router = express.Router(),
          CartController = require('../controllers/CartController')(CartModel, ProductModel)
  
    router.get('/:_id', CartController.readById.bind(CartController))  
    router.post('/', CartController.create.bind(CartController))
    router.post('/:_id', CartController.insertItem.bind(CartController))
    router.post('/checkout/:_id', CartController.checkout.bind(CartController))
    router.delete('/:_id/:_item', CartController.removeItem.bind(CartController))    
    
    return router
  }
  
  module.exports = createRouter