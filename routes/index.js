const express     = require('express'),
      router      = express.Router(),
      mongoose    = require('../db/mongoose'),
      ProductModel= require('../models/ProductModel')(mongoose),
      CartModel   = require('../models/CartModel')(mongoose),
      Products    = require('./products'),
      Carts       = require('./carts')

router.get('/', (request, response) => {
  response.send('Desafio backend REST api')
})

router.use('/produtos', Products(ProductModel))
router.use('/carrinhos',    Carts(CartModel, ProductModel))

module.exports = router