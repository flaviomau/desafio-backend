const debug = require('debug')('desafio-backend:controller'),
  Promise = require('bluebird'),
  Util = require('../utils/util'),
  axios = require('axios')

const buildCartItem = (body) => {
  return {
    produto: body.produto
  }
}

const buildTransaction = (list) => {
  return {
    "amount": 21000,
    "card_number": "4111111111111111",
    "card_cvv": "123",
    "card_expiration_date": "0922",
    "card_holder_name": "João das Neves",
    "customer": {
      "external_id": "#3311",
      "name": "João das Neves Braulio",
      "type": "individual",
      "country": "br",
      "email": "joaodasneves@got.com",
      "documents": [
        {
          "type": "cpf",
          "number": "00000000000"
        }
      ],
      "phone_numbers": ["+5511999998888", "+5511888889999"],
      "birthday": "1965-01-01"
    },
    "billing": {
      "name": "João das Neves",
      "address": {
        "country": "br",
        "state": "sp",
        "city": "Cotia",
        "neighborhood": "Rio Cotia",
        "street": "Rua Matrix",
        "street_number": "9999",
        "zipcode": "06714360"
      }
    },
    "shipping": {
      "name": "Neo Reeves",
      "fee": 1000,
      "delivery_date": "2000-12-21",
      "expedited": true,
      "address": {
        "country": "br",
        "state": "sp",
        "city": "Cotia",
        "neighborhood": "Rio Cotia",
        "street": "Rua Matrix",
        "street_number": "9999",
        "zipcode": "06714360"
      }
    },
    "items": list.map(item => {
      return {
        id: item._id,
        title: item.produto.descricao,
        unit_price: item.valor,
        quantity: 1,
        tangible: true
      }
    })
  }
}

function CartController(CartModel, ProductModel) {
  this.model = Promise.promisifyAll(CartModel)
  this.productModel = Promise.promisifyAll(ProductModel)
}

CartController.prototype.readById = function (request, response, next) {
  const _id = request.params._id
  let lista = {}
  let total = 0
  let items_counter = 0
  const model = this.model
  const discountTable = {
    A: { max: 5, perItem: 1 },
    B: { max: 15, perItem: 5 },
    C: { max: 30, perItem: 10 }
  }

  model.findOneAsync({ _id })
    .then(Util.handleNotFound)
    .then(function (data) {
      lista = data
      return model.countOneAsync(_id)
    })
    .then(counters => {
      return Object.keys(counters).map(key => {
        total += Number(counters[key].total)
        items_counter += counters[key].count
        const discount = discountTable[counters[key]._id].perItem * counters[key].count
        return discount < discountTable[counters[key]._id].max ? discount : discountTable[counters[key]._id].max
      }).reduce((a, b) => a + b, 0)
    })
    .then(value => {
      const discount = value < 30 ? value : 30
      const result = Object.assign({
        valorBruto: total,
        percentualDesconto: discount,
        valorLiquido: total - ((total * discount) / 100),
        quantidadeItens: items_counter
      }, lista._doc)
      response.json(Util.buildSuccessMessage('Dados do carrinho de compras recuperados com sucesso', result))
    })
    .catch(next)
}

CartController.prototype.create = function (request, response, next) {
  const item = buildCartItem(request.body)

  this.productModel.findOneAsync({ _id: item.produto })
    .then(Util.handleNotFound)
    .then(found => {
      /* Os valores das propriedades fator e valor são gravados no registro do carrinho, 
        pois caso ocorra uma atulização do registro do produto, os valores dos pedidos 
        serão mantidos */
      item['fator'] = found.fator
      item['valor'] = found.valor
      return {
        itens: [item]
      }
    }).then(cart => {
      return this.model.createAsync(cart)
    })
    .then(data => {
      response.json(Util.buildSuccessMessage('Registro de carrinho de compras criado com sucesso', data))
    })
    .catch(error => {
      if (error.errors) {
        const messages = Object.keys(error.errors).map(key => {
          return error.errors[key].message
        })
        next(Util.buildError(messages))
      } else {
        next(Util.buildError(error.message || error))
      }
    })
}

CartController.prototype.insertItem = function (request, response, next) {
  const _id = request.params._id
  const item = buildCartItem(request.body)

  this.productModel.findOneAsync({ _id: item.produto })
    .then(Util.handleNotFound)
    .then(found => {
      /* Os valores das propriedades fator e valor são gravados no registro do carrinho, 
        pois caso ocorra uma atualização do registro do produto, os valores dos pedidos 
        serão mantidos */
      item['fator'] = found.fator
      item['valor'] = found.valor
      return this.model.findOneAsync({ _id })
    })
    .then(cart => {
      cart.itens.push(item)
      return this.model.updateAsync(_id, cart)
    })
    .then(data => {
      response.json(Util.buildSuccessMessage('Novo item inserido no carrinho de compras com sucesso', { _id }))
    })
    .catch(error => {
      if (error.errors) {
        const messages = Object.keys(error.errors).map(key => {
          return error.errors[key].message
        })
        next(Util.buildError(messages))
      } else {
        next(Util.buildError(error.message || error))
      }
    })
}

CartController.prototype.removeItem = function (request, response, next) {
  const _id = request.params._id,
    _itemId = request.params._item

  this.model.findOneAsync({ _id })
    .then(Util.handleNotFound)
    .then(cart => {
      const itemCart = cart.itens.id(_itemId)
      if (itemCart === null) {
        throw Util.buildError('Item não localizado no carrinho de compras')
      } else {
        itemCart.remove()
        return cart.save()
      }
    }).then(data => {
      response.json(Util.buildSuccessMessage('Item removido do carrinho de compras com sucesso', { _id }))
    })
    .catch(error => {
      if (error.errors) {
        const messages = Object.keys(error.errors).map(key => {
          return error.errors[key].message
        })
        next(Util.buildError(messages))
      } else {
        next(Util.buildError(error.message || error))
      }
    })
}

CartController.prototype.checkout = function (request, response, next) {
  const _id = request.params._id,
        model = this.model,
        api_key = "ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd"

  model.findOneAsync({ _id })
    .then(Util.handleNotFound)
    .then(function (data) {      
      const transaction = buildTransaction(data.itens)
      return axios.post(`https://api.pagar.me/1/transactions?api_key=ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd`, transaction)      
    })
    .then(data => {
        response.json(Util.buildSuccessMessage('Checkout efetuado com sucesso', { _id }))
    })
    .catch(error => {    
      if (error.response && error.response.data && error.response.data.errors) {
        const messages = Object.keys(error.response.data.errors).map(key => {
          return error.response.data.errors[key].message
        })
        next(Util.buildError(messages))
      } else {
        next(Util.buildError(error.response || error.request || error.message || error))
      }
    })
}

module.exports = function (CartModel, ProductModel) {
  return new CartController(CartModel, ProductModel)
}