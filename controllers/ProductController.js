const debug = require('debug')('desafio-backend:controller'),
  Promise = require('bluebird'),
  Util = require('../utils/util')


function ProductController(ProductModel) {
  this.model = Promise.promisifyAll(ProductModel)
}

const buildProduct = (body) => {
  return {
    nome:       body.nome,
    descricao:  body.descricao,
    imagem:      body.imagem,
    valor:      body.valor,
    fator:     body.fator
  }
}

ProductController.prototype.readAll = function (request, response, next) {
  this.model.findAsync()
    .then(Util.handleNotFound)
    .then(function (data) {
      response.json(Util.buildSuccessMessage('Listagem de produtos realizada com sucesso', data))
    })
    .catch(next)
}

ProductController.prototype.readById = function (request, response, next) {
  const query = { _id: request.params._id }
  this.model.findOneAsync(query)
    .then(Util.handleNotFound)
    .then(function (data) {
      response.json(Util.buildSuccessMessage('Busca de registro de produto realizada com sucesso',data))
    })
    .catch(next)
}

ProductController.prototype.create = function (request, response, next) {
  const product = buildProduct(request.body)
  this.model.createAsync(product)
    .then(data => {
      response.json(Util.buildSuccessMessage('Registro de produto inserido com sucesso',data))
    })
    .catch(error => {
      if (error.errors) {
        const messages = Object.keys(error.errors).map(key => {
          return error.errors[key].message
        })
        next(Util.buildError(messages))
      } else {
        next(Util.buildError(error.message || error))
      }
    })
}

ProductController.prototype.update = function (request, response, next) {
  const _id = request.params._id
  const product = buildProduct(request.body)
  this.model.updateAsync(_id, product)
    .then(data => {
      response.json(Util.buildSuccessMessage('Registro de produto atualizado com sucesso', {_id}))
    })
    .catch(error => {
      if (error.errors) {
        const messages = Object.keys(error.errors).map(key => {
          return error.errors[key].message
        })
        next(Util.buildError(messages))
      } else {
        next(Util.buildError(error.message || error))
      }
    })
}

ProductController.prototype.remove = function (request, response, next) {
  const _id = request.params._id
  this.model.removeAsync(_id)
    .then(function (data) {
      response.json(Util.buildSuccessMessage('Registro de produto excluído com sucesso', {_id}))
    })
    .catch(next)
}

module.exports = function (ProductModel) {
  return new ProductController(ProductModel)
}